# Capybara::Finalize::Page

This gem injects the CSS and the images into the HTML code saved by capybara so
that HTML screenshots are pretty too.

The gem supports images (PNG, GIF and so on) and CSS. For the missing parts
please see the TODO section.

# TODO

 - [ ] Support SVG
 - [ ] Support Fonts (https://stackoverflow.com/questions/8749225/loading-an-external-font-via-inline-css)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'capybara-finalize-page'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install capybara-finalize-page

## Usage

Create the file `features/support/capybara_finalize_page.rb` and add the following:

```ruby
require 'capybara/finalize-page'
```

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/zedtux/capybara-finalize-page.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
