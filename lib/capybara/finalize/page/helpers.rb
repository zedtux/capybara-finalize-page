module Capybara
  module Helpers
    ##
    #
    # Updates the HTML before writing it to the temp file.
    #
    # @param [String] html                HTML code to inject into
    # @param [Capybara::Session] session  current Capybara session
    # @return [String]                    The modified HTML code
    #
    def finalise_html(html, session)
      html = inject_asset_host(html)
      html = inject_css_as_internal(html, session)
      html = inject_html_images_as_base64(html, session)
      html = inject_css_images_as_base64(html, session)
    end

    ##
    #
    # Injects the CSS fetched from the <link rel="stylesheet" ... /> as internal
    # CSS in the HTML
    #
    # @param [String] html                HTML code to inject into
    # @param [Capybara::Session] session  current Capybara session
    # @return [String]                    The modified HTML code
    #
    def inject_css_as_internal(html, session)
      rel_stylesheet = Nokogiri::HTML(html).css('link[rel="stylesheet"]')

      return html if rel_stylesheet.empty?

      rel_stylesheet = rel_stylesheet.attr('href').value
      session.visit(rel_stylesheet)
      css = session.driver.html

      html.gsub(%r{</head>}, "<style>#{css}</style></style>")
    end

    ##
    #
    #
    # Injects a Base64 version of the images in the <img> src attribute in the
    # HTML.
    # @param [String] html                HTML code to inject into
    # @param [Capybara::Session] session  current Capybara session
    # @return [String]                    The modified HTML code
    #
    def inject_html_images_as_base64(html, session)
      image_sources = Nokogiri::HTML(html).css('img').map do |img|
                        img.attr('src')
                      end

      image_sources.each do |img_src|
        begin
          session.visit(img_src)
          regex = %r{#{img_src}}
          image_base64 = Base64.encode64(session.driver.html)
          image_data = "data:image/gif;base64,#{image_base64}"
          html.gsub!(regex, image_data)
        rescue ActionController::RoutingError
          # Image cannot be downloaded, just ignore it.
        end
      end

      html
    end

    ##
    #
    # Injects a Base64 version of the images in the background:url() CSS rule
    # in the internal CSS.
    # @see #inject_css_as_internal
    #
    # @param [String] html                HTML code to inject into
    # @param [Capybara::Session] session  current Capybara session
    # @return [String]                    The modified HTML code
    #
    def inject_css_images_as_base64(html, session)
      image_urls = html.scan(/background(?:-image)?:\s?url\(([\/A-z\.\-0-9]+)\)/)
      image_urls.flatten.each do |img_url|
        extension = img_url.scan(/\.([A-z]+)$/).flatten.first

        # SVG needs some love in order to work
        # Something like "data:image/svg+xml;utf8,#{session.driver.html}"
        next if extension == 'svg'

        begin
          session.visit(img_url)
          regex = %r{#{img_url}}
          image_base64 = Base64.encode64(session.driver.html).gsub(%r{\n}, '')
          image_data = "data:image/#{extension};base64,#{image_base64}"
          html.gsub!(regex, image_data)
        rescue ActionController::RoutingError
          # Image cannot be downloaded, just ignore it.
        end
      end

      html
    end
  end
end
