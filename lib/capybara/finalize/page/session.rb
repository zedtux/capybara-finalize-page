require 'capybara/finalize/page/helpers'

module Capybara
  class Session
    ##
    #
    # Save a snapshot of the page. If `Capybara.asset_host` is set it will inject `base` tag
    #   pointing to `asset_host`.
    #
    # If invoked without arguments it will save file to `Capybara.save_path`
    #   and file will be given randomly generated filename. If invoked with a relative path
    #   the path will be relative to `Capybara.save_path`, which is different from
    #   the previous behavior with `Capybara.save_and_open_page_path` where the relative path was
    #   relative to Dir.pwd
    #
    # @param [String] path  the path to where it should be saved
    # @return [String]      the path to which the file was saved
    #
    def save_page(path = nil)
      path = prepare_path(path, 'html')
      File.write(path, Capybara::Helpers.finalise_html(body, self), mode: 'wb')
      path
    end
  end
end
